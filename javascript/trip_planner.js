let stateList = document.getElementById('state');
let activityList = document.getElementById('activity');
let limitList = document.getElementById('limit');
let submitButton = document.querySelector('#submit_button');
let plannerTable = document.getElementById('plannerTable');


for (let state in STATES) {
  let stateOption = document.createElement('option');
  stateOption.value = state;
  stateOption.textContent = STATES[state];
  stateList.appendChild(stateOption);
}


submitButton.addEventListener('click', () => {
  let selectedState = stateList.options[stateList.selectedIndex].value;
  let selectedActivity = activityList.options[activityList.selectedIndex].value;
  let selectedLimit = limitList.options[limitList.selectedIndex].value;
  let config = {
    method: 'get',
    url: corsAnywhere + recAreas + 'state=' + selectedState + '&activity=' + selectedActivity + '&limit=' + selectedLimit + '&sort=name',
    headers: APIOBJ
  };


  axios(config)
    .then(function (response) {
      const recDataReturned = response.data.RECDATA;
      let recDataTable = document.querySelector('.recDataTable');
      recDataTable.innerHTML = '';

      recDataReturned.forEach((location) => {
        let newTableRow = recDataTable.insertRow();
        let nameCell = newTableRow.insertCell(0);
        let numberCell = newTableRow.insertCell(1);
        let emailCell = newTableRow.insertCell(2);
        let mapCell = newTableRow.insertCell(3);
        let coordinates = location.GEOJSON.COORDINATES;
        let mapButton = document.createElement('button');


        mapButton.className = "mapButton";
        mapButton.innerText = "Map it!";
        mapCell.appendChild(mapButton);
        mapButton.onclick = function () {
          window.open(`https://www.google.com/maps?q=${coordinates[1]},${coordinates[0]}`, "_blank");
        }


        nameCell.innerText = location.RecAreaName;


        if (location.RecAreaPhone !== '{}' && location.RecAreaPhone !== '') {
          numberCell.innerText = location.RecAreaPhone;
        } else {
          numberCell.innerText = 'N/A';
        }


        if (location.RecAreaEmail !== '' && location.RecAreaEmail !== 'null') {
          emailCell.innerText = location.RecAreaEmail;
        } else {
          emailCell.innerText = 'N/A';
        }

        
        plannerTable.style.display = "table";
      });
    })
    .catch(function (error) {
      console.log(error);
    });
});