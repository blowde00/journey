let stateSelect = document.querySelector('#stateSelection');
let postRating = document.getElementById('post_rating');
let comments = document.getElementById('comments');
let ratingField = document.getElementById('rating');
let blogBody = document.getElementById('blog_posts');
let recAreaList = document.querySelector('#recArea');

for (let state in STATES) {
    let stateOption = document.createElement('option');
    stateOption.value = state;
    stateOption.textContent = STATES[state];
    stateSelect.append(stateOption);
};


stateSelect.addEventListener('change', (event) => {

    while (recAreaList.firstChild) {
        recAreaList.removeChild(recAreaList.firstChild);
    }


    let selectedState = event.target.value;
    let config = {
        method: 'get',
        url: corsAnywhere + recAreas + "state=" + selectedState + '&sort=name',
        headers: APIOBJ
    };


    axios(config)
        .then((response) => {
            let recArray = [];
            for (let recArea of response.data.RECDATA) {
                recArray.push(recArea.RecAreaName);
            }

            recArray.forEach(element => {
                let recLocation = document.createElement('option');
                recLocation.innerText = element;
                recAreaList.append(recLocation);
            });
        });
});


postRating.onclick = () => {
    let location = recAreaList.options[recAreaList.selectedIndex].value;
    let rating = ratingField.options[ratingField.selectedIndex].value;
    let newRow = blogBody.insertRow();
    let locationCell = newRow.insertCell(0);
    let commentsCell = newRow.insertCell(1);
    let ratingCell = newRow.insertCell(2);
    locationCell.innerText = location;
    commentsCell.innerText = comments.value;
    ratingCell.innerText = rating;
};